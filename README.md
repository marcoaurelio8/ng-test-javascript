# ProcessMaker Next Gen Javascript test.

In order to run the test suite first clone this repository and then install node.js, after that run
the following commands in the root location of this project:

    npm install
    npm start

You can then view the tests in your browser at
[http://localhost:4444](http://localhost:4444).

When you visit that page, all of the tests should be failing, your job is to
get the tests to pass. To do this, you'll need to edit the files in the `app/`
directory.
Once you update a test, you can reload the test page in the browser to see
whether it worked or not.
