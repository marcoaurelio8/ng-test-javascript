exports = (typeof window === 'undefined') ? global : window;

exports.functionsAnswers = {
  argsAsArray : function(fn, arr) {
    /*
      Helpers
      fn = function(greeting, name, punctuation) {
        sayItCalled = true;
        return greeting + ', ' + name + (punctuation || '!');
      };
    */
    return fn.apply(this, arr);
  },

  speak : function(fn, obj) {
    /*
      Helpers 
      var sayIt = function(greeting, name, punctuation) {
        sayItCalled = true;
        return greeting + ', ' + name + (punctuation || '!');
      };   
      var fn = function() {
          return sayIt(this.greeting, this.name, '!!!');
        };
     */
    return fn.call(obj);

  },

  functionFunction : function(str) {
    return function (str2) {
      return str + ", " + str2;
    }
  },

  makeClosures : function(arr, fn) {
    var r = [];
    arr.forEach(function (value, index, arr) {
      r.push(function () {
        return fn(value);
      });
    });
    return r;
  },

  partial : function(fn, str1, str2) {
    /**
     * Helpers
     * fn = function(greeting, name, punctuation) {
        sayItCalled = true;
        return greeting + ', ' + name + (punctuation || '!');
      };
     */
  },

  useArguments : function() {
    var i, r = null;
    for (i = 0; i < arguments.length; i++) {
      if (r === null) {
        r = arguments[i];
      } else {
        r += arguments[i];
      }
    }
    return r;
  },

  callIt : function(fn) {

  },

  partialUsingArguments : function(fn) {
    /*
      Helpers     
      var partialMe = function (x, y, z) {
        return x / y * z;
      };
     */
  },

  curryIt : function(fn) {
    /**
     * Helpers
     * var fn = function (x, y, z) {
        return x / y * z;
      };
     */
  }
};
