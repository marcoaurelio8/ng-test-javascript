exports = (typeof window === 'undefined') ? global : window;

exports.recursionAnswers = {
  permute: function(arr) {
      // take for example an array like:
      // var arr = [1 , 5 ,7];
      // executing the permute function
      // >> var result = permute(arr);
      // should return the following result:
      // [
      //    [1, 5, 7]
      //    [1, 7, 5]
      //    [5, 1, 7]
      //    [5, 7, 1]
      //    [7, 1, 5]
      //    [7, 5, 1]
      // ]
      var nlengh=valores.length;
    if (nlengh==1)
    {
        return;
    }
    for (var i=0;i<nlengh-1;i++)
    {
        for (var j=i+1; j<nlengh; j++)
        {
            var op1=valores[i], op2 =valores[j];
            valores.splice(j,1);
            valores.splice(i,1);
            for (var op=0; op<4;op++)
            {
              var res = calcula(op1,op2,op);
                if (res>0)
                {
                    solucionActual.push({op1:op1, op2:op2, op: op, res: res});
                    if (Math.abs(obtener-resultadoMejor)>Math.abs(obtener-res) || resultadoMejor==res && solucionMejor.length>solucionActual.length)
                    {
                        solucionMejor=solucionActual.slice(0);
                        resultadoMejor=res;
                    }
                    valores.push(res);
                    backtracking(valores);
                    valores.pop();

                    solucionActual.pop();
                }
            }
            valores.splice(i,0,op1);
            valores.splice(j,0,op2);
        }

    }

  },

  fibonacci: function(n) {
      // executing the function like:
      // fibonacci(2)
      // it should return
      // 1
      if(n <= 2) {
          return 1;
      } else {
          return this.fibonacci(n - 1) + this.fibonacci(n - 2);
      }
  },

  validParentheses: function(n) {
      // executing the function like:
      // validParentheses(3)
      // it should return an array
      // ['((()))', '(()())', '(())()', '()(())', '()()()']
      // all the valid combinations of three pairs of parentheses.
  }
};
