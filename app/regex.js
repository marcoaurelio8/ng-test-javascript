exports = (typeof window === 'undefined') ? global : window;

exports.regexAnswers = {
  containsNumber : function(str) {
      // 'abc123' should return true
      // 'abc' should return false
      return /\d+/.test(str);
  },

  containsRepeatingLetter : function(str) {
      // 'aabc' should return true
      // 'abc' should return false
      return /([A-Za-z]).*?\1/.test(str);
  },

  endsWithVowel : function(str) {
      // 'something' should return false
      // 'create' should return true
      var r = new RegExp('[aeiou]$', 'i');
      return r.test(str);
  },

  isUSD : function(str) {
      // for example:
      // '$132.03' should be true
      // '$132,219' should be true
      // '$132,212.43' should be true
      // '$132.034,233' should be false
      // '$132.034_23' should be false
      // '$132.013,14.23' should be false
      return /^\$\d{1,3}(,\d{3})*(\.\d{2})?$/.test(str);
  }
};
