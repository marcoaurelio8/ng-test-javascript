exports = (typeof window === 'undefined') ? global : window;

exports.arraysAnswers = {

  indexOf : function(arr, item) {
    return arr.indexOf(item);
  },

  sum : function(arr) {
      var r = 0;
    arr.forEach(function(value, pos, arr) {
      r += value;
    });
    return r;
  },

  remove : function(arr, item) {
      arr.forEach(function(value, pos, arr) {
      if (value === item) {
        arr.splice(pos, 1);
      }
    });
    return arr;
  },

  removeWithoutCopy : function(arr, item) {
    var i;
    for (i = 0; i < arr.length; i++) {
      if (arr[i] === item) {
        arr.splice(i, 1);
        i--;
      }
    }
    return arr;
  },

  append : function(arr, item) {
    arr.push(item);
    return arr;
  },

  truncate : function(arr) {
    arr.pop();
    return arr;
  },

  prepend : function(arr, item) {
    arr.unshift(item);
    return arr;
  },

  curtail : function(arr) {
    arr.shift();
    return arr;
  },

  concat : function(arr1, arr2) {
    return arr1.concat(arr2);
  },

  insert : function(arr, item, index) {
    var arr1 = arr.slice(0, index),
        arr2 = arr.slice(index);

    return arr1.concat([item], arr2);
  },

  count : function(arr, item) {
    var c = 0;
    arr.forEach(function (value, pos, arr) {
      if (item === value) {
        c++;
      }
    });
    return c;
  },

  duplicates : function(arr) {
    var duplicates = [], that = this;
    arr.forEach(function (value, por, arr) {
      if (that.count(arr, value) > 1 && that.indexOf(duplicates, value) === -1) {
        duplicates.push(value);
      }
    });
    return duplicates;
  },

  square : function(arr) {
    var r = [];
    arr.forEach(function (value, por, arr) {
      r.push(Math.pow(value, 2));
    });
    return r;
  },

  findAllOccurrences : function(arr, target) {
    var i,
    size = arr.length,
    count = 0,
    newArray = [];
    for (i = 0; i < size; i += 1) {
      if (target === arr[i]) {
          newArray.push(i);
      }
    }
    return newArray;
  }
};
