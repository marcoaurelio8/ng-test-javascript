exports = (typeof window === 'undefined') ? global : window;

exports.objectsAnswers =  {
  alterContext : function(fn, obj) {
      // Consider an object a like:
      // a = {
      //     name : 'Matt',
      //     greeting : 'Hello',
      //     sayIt : function() {
      //         return this.greeting + ', ' +
      //         this.name + '!';
      //     }
      // };
      // 
      // Consider a object b like:
      // b = {
      //   name : 'Rebecca',
      //   greeting : 'Yo'
      // };
      //
      // The result executing
      // >> alterContext(a, b)
      // should be:
      // >> Yo, Rebecca
      // altering the context of the a.name and a.gretting attributes
      return fn.call(obj);
  },

  alterObjects : function(constructor, greeting) {
      // Consider an object C
      // C = function(name) {
      //    this.name = name;
      //    return this;
      // };
      // and 2 instances of C
      // obj1 = new C('Carol');
      // obj2 = new C('Mellisa');
      // greeting = 'What\'s up';
      // executing: alterObjects(C, greeting)
      // should add that attribute to obj1 and obj2
      // Example obj1.greeting should be 'What\'s up'
      constructor.prototype.greeting = greeting;
  },

  iterate : function(obj) {
      // Consider an object definition like
      // var C = function() {
      //     this.foo = 'one';
      //     this.baz = 'two';
      // };
      //
      // C.prototype.something = 'three';
      // C.prototype.anything = 'four';
      //
      // apply the function:
      // var obj = new C();
      // >> iterate(obj)
      // should return an array like
      // ['foo: one','baz: two']
      var r = [];
      for(key in obj) {
        if (obj.hasOwnProperty(key)) {
          r.push(key + ": " + obj[key]);
        }
      }
      return r;
  }
};
